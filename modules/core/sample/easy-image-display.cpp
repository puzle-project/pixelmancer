//
// Created by cedric-cf on 07/05/18.
//


#include <vector>
#include <window.h>
#include <gl_wrapper/texture.h>
#include <engine.h>
#include <geometry/square.h>
#include <opencv2/core/hal/interface.h>
#include <opencv2/core.hpp>
#include <opencv2/opencv.hpp>

std::shared_ptr<texture_2D<uchar>> load_2D_texture(const std::string & file_path) {
    cv::Mat image_to_display = cv::imread("./assets/image.jpg");
    cv::Mat flipped_image(image_to_display);

    cv::flip(image_to_display, flipped_image, 0);

    return std::make_shared(texture_2D<uchar>(
            image_to_display.ptr(),
            GL_BGR,
            GL_RGB,
            size_t(image_to_display.cols),
            size_t(image_to_display.rows)
    ));
}

int main (int argc, char ** argv) {

    window w(800, 600, "Title");
    w.init();

    engine e (60);

    std::vector<square> squares;

    auto texture = load_2D_texture("./assets/image.jpg");

    //scene_2d scene (800, 600);

    for (size_t i = 0; i < 10; ++i) {
        square sq (0, 0, 80,60);

        sq.set_full_texture(texture);
        squares.emplace_back(sq);

    }

    e.loop([&]() {
        return w.should_close();
    }, [&]() {
        //glClearColor(0.0, 0.8, 0.3, 1.0);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        std::for_each(squares.begin(), squares.end(), [](auto & item) {
           item.display();
        });
        w.swap_buffers();
        glfwPollEvents();
    });


    return 1;
}
