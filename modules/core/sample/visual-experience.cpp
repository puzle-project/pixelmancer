//
// Created by cedric-cf on 27/04/18.
//

#include <chrono>
#include <thread>
#include <string>
#include <opencv2/opencv.hpp>
#include <zconf.h>

int main () {




    std::vector<std::string> to_load = {
            "./assets/image.jpg",
            "./assets/image_2.jpg",
    };

    /*
    for(size_t i = 1; i <= 12; ++i) {
        std::stringstream filepath;
        filepath << "./assets/image" << i << ".jpg";
        to_load.push_back(filepath.str());
    }
    */

    std::vector<cv::Mat> images;

    std::for_each(to_load.begin(), to_load.end(), [&](auto file_path)  {
        std::cout << file_path << std::endl;

        images.emplace_back(cv::imread(file_path));
    });

    std::string w_name("VisualExperience");

    cv::namedWindow(w_name, CV_WINDOW_NORMAL);
    cv::setWindowProperty(w_name, CV_WND_PROP_FULLSCREEN, CV_WINDOW_FULLSCREEN);

    size_t index = 0;

    cv::imshow(w_name, images[0]);

    usleep(1000 * 5000);
    cv::imshow(w_name, images[1]);

    cv::waitKey(0);
    /*
    for(;;) {

        cv::imshow(w_name, images[index]);
        if (++index > 1) {
            index =0;
        }
        std::cout << index << std::endl;

        std::this_thread::sleep_for(std::chrono::milliseconds(5000));
    }
     */
}