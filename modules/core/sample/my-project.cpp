#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include <window.h>
#include <engine.h>
#include <visualizer.h>

#include <opencv2/opencv.hpp>


int main(int argc, char** argv)
{

    window w (800, 600, "Title");
    w.init();

    engine e (60);

    cv::Mat image_to_display = cv::imread("./assets/image.jpg");
    cv::Mat flipped_image(image_to_display);

    cv::flip(image_to_display, flipped_image, 0);

    texture_2D<uchar> to_display(
            image_to_display.ptr(),
            GL_BGR,
            GL_RGB,
            size_t(image_to_display.cols),
            size_t(image_to_display.rows)
    );

    visualizer visu;
    visu.init ();
    visu.set_texture(
            image_to_display.ptr(),
            size_t(image_to_display.cols),
            size_t(image_to_display.rows)
    );


    e.loop([&]() {
        return w.should_close();
    }, [&]() {
        //glClearColor(0.0, 0.8, 0.3, 1.0);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        visu.display();
        w.swap_buffers();
        glfwPollEvents();
    });

    return 0;
}