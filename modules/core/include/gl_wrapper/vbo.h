//
// Created by cedric-cf on 25/04/18.
//

#ifndef CPP_SEED_VBO_H
#define CPP_SEED_VBO_H

#include <GL/glew.h>
#include <memory>
#include "shader.h"
#include "get_gl_type.h"


template <typename T, GLenum Buffer_Type, template <typename, typename ...> class Container>
class vbo {

     GLuint id;

public:
    std::shared_ptr<Container<T>> host_data;

    vbo ():
        id()
    {
        glGenBuffers (1, &id);
    }

    ~vbo () {
        glDeleteBuffers(1, &id);
    }

    GLuint get_id () const {
        return id;
    }

    GLuint size () {
        return host_data->size();
    }

    void bind () {
        glBindBuffer (Buffer_Type, id);
    }

    void unbind() {
        glBindBuffer(Buffer_Type, 0);
    }

    void data(Container<T> * host_data, GLenum usage = GL_STATIC_DRAW) {
        this->bind();
        this->host_data.reset(host_data);
        glBufferData(Buffer_Type, host_data->size() * sizeof(T), (const void *)(host_data->data()), usage);
    }

    void data (const Container<T> & host_data, GLenum usage = GL_STATIC_DRAW) {
        this->bind();
        this->host_data = std::make_shared<Container<T>>(Container<T>(host_data));
        glBufferData(Buffer_Type, host_data.size() * sizeof(T), (const void *)(host_data.data()), usage);
    }

    void attrib_pointer (GLuint index, GLint size, GLboolean normalized, GLsizei stride, const GLvoid * pointer) {
        glVertexAttribPointer(index, size, get_gl_enum<T>(), normalized, stride, pointer);
    }
};

void vbo_unbind(GLenum usage) {
    glBindBuffer(0, usage);
}
#endif //CPP_SEED_VBO_H
