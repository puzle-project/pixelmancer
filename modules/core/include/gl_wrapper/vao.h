//
// Created by cedric-cf on 25/04/18.
//

#ifndef CPP_SEED_VAO_H
#define CPP_SEED_VAO_H

#include <GL/glew.h>
#include <utility>
#include <vector>
#include "vbo.h"

class vao {
    GLuint id;
    GLuint vbo_counter;
public:
    vao ():
        id(),
        vbo_counter(0) {
        glGenVertexArrays(1, &id);
        std::cout <<glGetError()<<std::endl;
    }

    void bind () {
        glBindVertexArray (id);
    }

    void unbind () {
        glBindVertexArray (0);
    }

    void enable_vertex_attrib_array(GLuint index) {
        //glEnableVertexArrayAttrib(id, index);
        bind();
        glEnableVertexAttribArray(index);
    }

    /* TODO:
     * - unlink method
     * - map usage to store each linked VBO
     */


};



#endif //CPP_SEED_VAO_H
