//
// Created by cedric-cf on 27/04/18.
//

#ifndef CPP_SEED_GET_GL_TYPE_H
#define CPP_SEED_GET_GL_TYPE_H


#include <GL/gl.h>

template<typename E>   // primary template
constexpr GLenum get_gl_enum()=delete;

template <>
constexpr GLenum get_gl_enum<GLfloat>(){
    return GL_FLOAT;
}

template <>
constexpr GLenum get_gl_enum<GLint>(){
    return GL_INT;
}

template <>
constexpr GLenum get_gl_enum<GLshort>() {
    return GL_SHORT;
}

template <>
constexpr GLenum get_gl_enum<GLdouble>() {
    return GL_DOUBLE;
}

template <>
constexpr GLenum get_gl_enum<GLuint>() {
    return GL_UNSIGNED_INT;
}

template <>
constexpr GLenum get_gl_enum<GLubyte> () {
    return GL_UNSIGNED_BYTE;
}


#endif //CPP_SEED_GET_GL_TYPE_H
