//
// Created by cedric-cf on 27/04/18.
//

#ifndef CPP_SEED_TEXTURE_H
#define CPP_SEED_TEXTURE_H

#include <GL/gl.h>
#include <algorithm>
#include "get_gl_type.h"

//TODO: Template version with Dimension a parameter
template <typename T_Value_Type>
class texture_2D {

    using Value_Type = T_Value_Type;

    static const GLenum _texture_type = GL_TEXTURE_2D;
    GLuint _id;

    void * _host_data;
    GLenum _host_format;
    GLenum _device_format;
    size_t _width, _height;
    bool _border;
    GLint _level;

public:

    texture_2D() = default;

    texture_2D (
            void * host_data,
            GLenum host_format,
            GLenum device_format,
            size_t width,
            size_t height,
            bool border = false
    ) :
        _host_data(host_data),
        _host_format(std::move(host_format)),
        _device_format(std::move(device_format)),
        _width(std::move(width)),
        _height(std::move(height)),
        _border(border),
        _level(0)
    {
        glGenTextures(1, & _id);
    }

    GLuint get_id () const {
        return _id;
    }

    void bind () {
        glBindTexture(_texture_type, _id);
    }

    //TODO: generic tex_parameter with template specialization
    void tex_parameter(GLenum parameter_name, GLint value) {
        glTexParameteri(_texture_type, parameter_name, value);
    }

    void pixel_store(GLenum name, GLint value) {
        glPixelStorei(name, value);
    }

    void tex_image(GLint level) {
        glTexImage2D(
                _texture_type,
            level,
             _device_format,
             _width,
             _height,
             _border? 1:0,
             _host_format,
             get_gl_enum<Value_Type>(),
            (Value_Type *)(_host_data)
        );
    }



    void tex_image() {
        tex_image(_level);
    }

    void generate_mipmap() {
        glGenerateMipmap(_texture_type);
    }

};

#endif //CPP_SEED_TEXTURE_H
