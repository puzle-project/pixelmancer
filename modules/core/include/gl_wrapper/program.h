//
// Created by cedric-cf on 26/04/18.
//

#ifndef CPP_SEED_PROGRAM_H
#define CPP_SEED_PROGRAM_H

#include <GL/glew.h>
#include <stdexcept>
#include "shader.h"

class program {

    GLuint id;

public:
    program():
        id(0)
    {}

    ~program () {
        this->destroy();
    }


    GLuint get_id() const {
        return id;
    }

    void destroy () {
        if (glIsProgram(id) == GL_TRUE) {
            glDeleteProgram;
        }
    }

    void create () {
        id = glCreateProgram ();

        if (!id) {
            throw std::runtime_error ("An error occurs during the program creation");
        }
    }

    void link () {
        try {
            glLinkProgram(id);
            this->throw_error ();
        } catch (const std::runtime_error & error) {
            std::cerr << error.what() <<std::endl;
            this->destroy ();
        }
    }

    void throw_error () {
        GLint error_size(0);
        char * error_msg;
        glGetProgramiv(id, GL_INFO_LOG_LENGTH, &error_size);
        error_msg = new char[error_size + 1];
        glGetProgramInfoLog(id, error_size, &error_size, error_msg);
        error_msg[error_size] = '\0';

        std::string error(error_msg);
        delete[] error_msg;

        throw std::runtime_error(error);
    }

    void attach_shader(const shader & to_attach) {
        glAttachShader(id, to_attach.get_id());
    }

    void bind_attrib_location (GLuint index, const GLchar * name) {
        glBindAttribLocation(id, index, name);
    }

    GLuint get_attrib_location (const GLchar * name) {
        GLint index = glGetAttribLocation(get_id(), name);

        if (index < 0) {
            throw std::runtime_error("Invalid attrib location name: "+ std::string(name));
        }
        return GLuint(index);
    }

    GLuint get_uniform_location(const GLchar* name) {
        GLint index = glGetUniformLocation(get_id(), name);

        if (index < 0) {
            throw std::runtime_error("Invalid uniform location name");
        }
        return GLuint(index);
    }

    void bind_frag_data_location(GLuint color_number, const GLchar* name) {
        glBindFragDataLocation(get_id(), color_number, name);
    }

    void use() {
        glUseProgram(id);
    }

    void unuse () {
        glUseProgram(0);
    }
};

#endif //CPP_SEED_PROGRAM_H
