//
// Created by cedric-cf on 26/04/18.
//

#ifndef CPP_SEED_SHADER_H
#define CPP_SEED_SHADER_H

#include <experimental/filesystem>
#include <GL/glew.h>
#include <fstream>
#include <iostream>
#include <gl_wrapper/GL_Error.h>
#include <cassert>

namespace fs = std::experimental::filesystem;

class shader {

    GLuint id;
    GLenum type;

public:

    shader (GLenum type):
        type(std::move(type)) {

    }

    const GLuint get_id () const{
        return id;
    }

    fs::path file_path;
    shader(fs::path file_path, GLenum type):
        file_path(std::move(file_path)),
        type(std::move(type))
    {}

    ~shader () {
        destroy();
    }

    std::string get_sources(const fs::path & file_path) const{

        std::cout << fs::absolute(file_path) << std::endl;
        std::cout << fs::current_path() << std::endl;

        auto status = fs::status(fs::current_path() / file_path);
        //std::cout << status.type() << std::endl;
        std::ifstream file(fs::absolute(file_path).string());

        if (!file) {
            std::cout << fs::absolute(file_path) << std::endl;
            throw std::runtime_error("Cannot open the sources" + fs::absolute(file_path).string());
        }

        std::stringstream sources;
        std::string line;

        while (getline (file, line)) {
            sources << line << std::endl;
            std::cout << line << std::endl;
        }

        file.close();

        return sources.str();
    }


    void throw_error () {
        GLint error_size(0);
        char * error_msg;
        glGetShaderiv(id, GL_INFO_LOG_LENGTH, &error_size);
        error_msg = new char[error_size + 1];
        glGetShaderInfoLog(id, error_size, &error_size, error_msg);
        error_msg[error_size] = '\0';

        std::string error(error_msg);
        delete[] error_msg;

        throw std::runtime_error(error);
    }

    void check_compile_error () {
        GLint compile_error(0);

        glGetShaderiv(id, GL_COMPILE_STATUS, &compile_error);

        if (compile_error != GL_TRUE) {
            throw_error ();
        }
    }

    void load ( const char * source_code) {

        id = glCreateShader(type);


        if (!id) {
            throw std::runtime_error("An error occurs during the shader creation");
        }

        glShaderSource(id, 1, &source_code, 0);

        try {
            glCompileShader(id);
            this->check_compile_error();

        } catch (const std::runtime_error & error) {
            std::cerr << error.what() << std::endl;
            glDeleteShader(id);
        }
    }

    void load_from_file(const fs::path & source_path) {
        std::string source_code_string = std::move(get_sources(source_path));
        const char * source_code = source_code_string.c_str();
        std::cout << "Source code" << source_code << std::endl;
        this->load(source_code);
    }

    void load_as_previous () {
        this->load_from_file(file_path);
    }


    void destroy () {
        if (glIsShader(id) == GL_TRUE) {
            glDeleteShader(id);
        }
    }

    void bind_attrib_location(GLuint index, GLchar * name) {
        glBindAttribLocation(id, index, name);
    }
};


#endif //CPP_SEED_SHADER_H
