//
// Created by cedric-cf on 24/04/18.
//

#ifndef CPP_SEED_WINDOW_H
#define CPP_SEED_WINDOW_H

#include <GLFW/glfw3.h>
#include <string>
#include <stdexcept>
#include <GL/glu.h>
#include <GL/glew.h>


// TODO: better use a functor
void controls(GLFWwindow* window, int key, int scancode, int action, int mods)
{
    if(action == GLFW_PRESS)
        if(key == GLFW_KEY_ESCAPE)
            glfwSetWindowShouldClose(window, GL_TRUE);
}

class window {

    int w, h;
    std::string title;
    int x, y;

public:

    GLFWwindow* glfw_window;

    window(
        int w,
        int h,
        std::string title,
        int x = 0,
        int y = 0) :
            w(w),
            h(h),
            title(std::move(title)),
            x(x),
            y(y),
            glfw_window(nullptr)
    {}

    ~window() {
        glfwDestroyWindow(glfw_window);
        glfwTerminate();
    }

    void init() {
        if(!glfwInit()) {
            throw std::runtime_error("Cannot initialize GLFW");
        }

        glfwWindowHint(GLFW_SAMPLES, 4);
        glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
        glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
        glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

        glfw_window = glfwCreateWindow(w, h, title.c_str(), NULL, NULL );

        if(!glfw_window) {
            throw std::runtime_error("Cannot create the window");
        }
        glfwMakeContextCurrent(glfw_window);
        glfwSetKeyCallback(glfw_window, controls);
        glfwSetWindowSize(glfw_window, w, h);

        glewExperimental = GL_TRUE;
        glewInit();

        glEnable(GL_DEPTH_TEST); // Depth Testing
        glDepthFunc(GL_LEQUAL);
        glDisable(GL_CULL_FACE);
        //glCullFace(GL_BACK);

    }

    int should_close() {
        return glfwWindowShouldClose(glfw_window);
    }

    void swap_buffers() {
        glfwSwapBuffers(glfw_window);
    }

};

#endif //CPP_SEED_WINDOW_H
