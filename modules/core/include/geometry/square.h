//
// Created by cedric-cf on 07/05/18.
//

#ifndef CPP_SEED_SQUARE_H
#define CPP_SEED_SQUARE_H

#include <glm/detail/type_mat.hpp>
#include <glm/detail/type_mat4x4.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <bits/shared_ptr.h>
#include <gl_wrapper/texture.h>
#include <gl_wrapper/program.h>



class square {
    float x;
    float y;
    float width;
    float height;


    std::shared_ptr<program> _program;
    std::shared_ptr<texture_2D<GLubyte>> texture;
    glm::mat4 model;
public:
    square(float x, float y, float width, float height,
       std::shared_ptr<texture_2D> texture,
       std::shared_ptr<program> program
    ) :
        x(x),
        y(y),
        width(width),
        height(height),
        texture(std::move(texture)),
        _program(std::move(program))
    {

        model = glm::mat4(1);
        model = glm::scale(model, glm::vec3(width, height, 1));
        model = glm::translate(model, glm::vec3(x, y, 0));
    }


    void translate (float x, float y, float z = 0) {
        model = glm::translate(model, glm::vec3(x, y, z));
    }

    square& operator*=(const square & to_transform) {
        model *= to_transform.model;
        return *this;
    }

    square& operator*=(const glm::mat4 & transformation) {
        model *= transformation;
        return *this;
    }

    void display(glm::mat4 PV) {
        geometry.display(PV * model);
    }

};

square& operator*(square& to_transform, const glm::mat4& transformation) {
    return to_transform *= transformation;
}

#endif //CPP_SEED_SQUARE_H
