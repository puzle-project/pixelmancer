//
// Created by cedric-cf on 25/04/18.
//

#ifndef CPP_SEED_VISUALIZER_H
#define CPP_SEED_VISUALIZER_H

#include "gl_wrapper/vao.h"
#include "gl_wrapper/shader.h"
#include "gl_wrapper/program.h"
#include "gl_wrapper/texture.h"

#include <experimental/filesystem>

#include <glm/mat4x4.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <GLFW/glfw3.h>
#include <iomanip>
#include <opencv2/core/hal/interface.h>

void PrintOpenGLErrors(char const * const Function, char const * const File, int const Line)
{
    bool Succeeded = true;

    GLenum Error = glGetError();
    if (Error != GL_NO_ERROR)
    {
        char const * ErrorString = (char const *) gluErrorString(Error);
        if (ErrorString)
            std::cerr << ("OpenGL Error in %s at line %d calling function %s: '%s'", File, Line, Function, ErrorString) << std::endl;
        else
            std::cerr << ("OpenGL Error in %s at line %d calling function %s: '%d 0x%X'", File, Line, Function, Error, Error) << std::endl;
    }
}


#define CheckedGLCall(x) do { PrintOpenGLErrors(">>BEFORE<< "#x, __FILE__, __LINE__); (x); PrintOpenGLErrors(#x, __FILE__, __LINE__); } while (0)
#define CheckedGLResult(x) (x); PrintOpenGLErrors(#x, __FILE__, __LINE__);
#define CheckExistingErrors(x) PrintOpenGLErrors(">>BEFORE<< "#x, __FILE__, __LINE__);


void PrintShaderInfoLog(GLint const Shader)
{
    int InfoLogLength = 0;
    int CharsWritten = 0;

    glGetShaderiv(Shader, GL_INFO_LOG_LENGTH, & InfoLogLength);

    if (InfoLogLength > 0)
    {
        GLchar * InfoLog = new GLchar[InfoLogLength];
        glGetShaderInfoLog(Shader, InfoLogLength, & CharsWritten, InfoLog);
        std::cout << "Shader Info Log:" << std::endl << InfoLog << std::endl;
        delete [] InfoLog;
    }
}



class visualizer {

    vao _vao;
    vbo<GLfloat, GL_ARRAY_BUFFER, std::vector>   _vertices;
    vbo<GLfloat, GL_ARRAY_BUFFER, std::vector>   _colors;
    vbo<GLfloat, GL_ARRAY_BUFFER, std::vector>   _uv;
    vbo<GLuint, GL_ELEMENT_ARRAY_BUFFER, std::vector>_indices;

    shader _vertex_shader;
    shader _fragment_shader;

    program _program;

    texture_2D<GLubyte> _texture;

    GLuint _tex_id;

    public:

    visualizer():
        _vertex_shader(GL_VERTEX_SHADER),
        _fragment_shader(GL_FRAGMENT_SHADER)
    {}

    void set_texture(uchar * image_data, size_t width, size_t height) {
        _texture = texture_2D<GLubyte>(
                (void*)(image_data),
                GL_BGR,
                GL_RGB,
                width,
                height
        );

        _texture.bind();
        _texture.tex_parameter(GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        _texture.tex_parameter(GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        _texture.tex_parameter(GL_TEXTURE_WRAP_S, GL_CLAMP);
        _texture.tex_parameter(GL_TEXTURE_WRAP_T, GL_CLAMP);

        _texture.pixel_store(GL_UNPACK_ALIGNMENT, 1);
        _texture.pixel_store(GL_UNPACK_ROW_LENGTH, 0);
        _texture.pixel_store(GL_UNPACK_SKIP_PIXELS, 0);
        _texture.pixel_store(GL_UNPACK_SKIP_ROWS, 0);

        _texture.tex_image();
        _texture.generate_mipmap();

    }

    void create_square (size_t width, size_t height) {

        const std::vector<GLfloat> v = {
                0.0f,  0.f,
                0.f, height,
                width, height,
                width, 0.f
        };

        const std::vector<GLfloat> c = {
                1, 1, 0,
                0, 0, 1,
                0, 1, 0,
                1, 0, 0
        };

        const std::vector<GLuint> i = {
                0,
                1,
                2,

                0,
                2,
                3,
        };

        const std::vector<GLfloat> uv = {
                0, 0,
                0, 1,
                1, 1,
                1, 0
        };

        _vao.bind();

        _vertices.data(v, GL_STATIC_DRAW);
        _colors.data(c, GL_STATIC_DRAW);
        _uv.data(uv, GL_STATIC_DRAW);

        _indices.data(i);

        vbo_unbind(GL_ARRAY_BUFFER);
        namespace fs = std::experimental::filesystem;

        _vertex_shader.load_from_file(fs::path("shaders/forme2D.vert"));
        _fragment_shader.load_from_file(fs::path("shaders/forme2D.frag"));

        _program.create();
        _program.attach_shader(_vertex_shader);
        _program.attach_shader(_fragment_shader);

        _program.bind_attrib_location(0, "outColor");

        _program.link();
        _program.use();


        GLuint location_position = _program.get_attrib_location("in_position");
        _vao.enable_vertex_attrib_array(location_position);
        _vertices.bind();
        _vertices.attrib_pointer(location_position, 2, GL_FALSE, 0, 0);

        GLuint location_color = _program.get_attrib_location("in_color");
        _vao.enable_vertex_attrib_array(location_color);
        _colors.bind();
        _colors.attrib_pointer(location_color, 3, GL_FALSE, 0, 0);

        GLuint location_uv = _program.get_attrib_location("in_uv");
        _vao.enable_vertex_attrib_array(location_uv);
        _uv.bind();
        _uv.attrib_pointer(location_uv, 2, GL_FALSE, 0, 0);

        vbo_unbind(GL_ARRAY_BUFFER);
        vbo_unbind(GL_ELEMENT_ARRAY_BUFFER);
    }

    void init () {
        this->create_square (800.f, 600.f);
    }

    void display () {
        glm::mat4 P = glm::ortho(0.f, 800.f, 0.f, 600.f, 0.1f, 100.f);
        //glm::mat4 P = glm::perspective(90.f, 4.f/3.f, 0.01f, 1500.f);
        glm::mat4 VM = glm::lookAt(
                glm::vec3(0.f, 0.f, 5.f),
                glm::vec3(0.f, 0.f, -1.f),
                glm::vec3(0.f, 1.f, 0.f)
        );

        glm::mat4 pvm = P * VM;

        glUniformMatrix4fv(
            glGetUniformLocation(_program.get_id(), "pvm"),
            1,
            GL_FALSE,
            glm::value_ptr(pvm)
        );

        glDrawElements(GL_TRIANGLES, _indices.size(), GL_UNSIGNED_INT, 0);

    }


};

#endif //CPP_SEED_VISUALIZER_H
