//
// Created by cedric-cf on 24/04/18.
//

#ifndef CPP_SEED_ENGINE_H
#define CPP_SEED_ENGINE_H

#include <chrono>
#include <functional>
#include <iostream>

template <typename Head, typename ... Args>
void run(const Head & head, Args && ... args) {
    head();
    run(std::forward<Args>(args)...);
};

template <typename Head>
void run(const Head & head) {
    head();
}

class engine {

public:

    float fps;
    float delta;

    engine(float fps=60):
        fps(fps),
        delta(1000/fps){}

    template <typename Should_Close, typename ... Args>
    void loop(const Should_Close & should_close, Args && ... to_run) {
        namespace sc = std::chrono;

        while(!should_close()) {
            auto current_tick = std::chrono::system_clock::now();

            run(std::forward<Args>(to_run)...);

            while(sc::duration_cast<sc::milliseconds>(sc::system_clock::now() - current_tick).count() < delta);
        }
    }

};

#endif //CPP_SEED_ENGINE_H
