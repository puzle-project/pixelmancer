#version 130
in vec3 in_color;
in vec2 in_position;
in vec2 in_uv;


uniform mat4 pvm;

out vec3 mid_color;
out vec2 UV;

void main() {
    mid_color = in_color;
    gl_Position = pvm * vec4(in_position, 1.0, 1.0);
    UV = in_uv;
}
