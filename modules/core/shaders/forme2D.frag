#version 130

in vec3 mid_color;
in vec2 UV;

out vec4 outColor;

uniform sampler2D texture_sampler;

void main() {
    outColor = 
        vec4(texture(texture_sampler, UV).rgb, 1) 
        + 0.2 
        * vec4(mid_color, 1);
}